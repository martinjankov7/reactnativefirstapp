import React, { Component } from 'react';
import { AppRegistry } from 'react-native';

import ReactNativeFirstApp from './App/containers/app';

AppRegistry.registerComponent('ReactNativeFirstApp', () => ReactNativeFirstApp);