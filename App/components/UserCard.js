import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';

export default class UserCard extends Component {

	constructor(props) {
	    super(props);
	}

	render(){
		let user = this.props.data;
		if(Object.keys(user).length){
			return (
				<View>
					<Image style={styles.image} source={{ uri: user.picture.large  }} />
					<Text style={styles.name}>{user.name.first} {user.name.last}</Text>	
					<Text style={styles.info}>Street: {user.location.street}</Text>	
					<Text style={styles.info}>City: {user.location.city}</Text>	
					<Text style={styles.info}>Email: {user.email}</Text>	
				</View>
			);
		}
		return false;
	}
}

const styles = {
	image: {
		resizeMode: 'contain',
    	width: null,
    	height: 300
	},
	name: {
		fontSize: 24
	},
	info: {
		fontSize: 14
	}
}