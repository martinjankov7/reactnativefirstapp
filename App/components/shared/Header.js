import React from 'react';
import { View, Text } from 'react-native';

const Header = ({ headerText }) => {
	const { headerStyle, textStyle } = styles;
	return(
		<View style={headerStyle}>
			<Text style={textStyle}>{headerText}</Text>
		</View>
	);
}

const styles = {
	headerStyle : {
		height: 80,
		backgroundColor: '#ccc',
		alignItems: 'center',
		justifyContent: 'center',
		shadowColor: "#000",
	    shadowOpacity: 0.2,
	    shadowOffset: {
	      height: 2,
	      width: 0
	    }
	},
	textStyle : {
		fontSize: 20,
		color: '#000'
	}
}

export default Header;