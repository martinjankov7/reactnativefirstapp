import React, { Component } from 'react';
import { View, ActivityIndicator, Button } from 'react-native';

import Header from '../components/shared/Header';
import Card from '../components/shared/Card';
import CardSection from '../components/shared/CardSection';
import Spinner from '../components/shared/Spinner';
import UserCard from '../components/UserCard';

export default class ReactNativeFirstApp extends Component {
  constructor(){
    super();

    this.state = {
      user : {},
      loading: true
    };

  }

  getUser(){
    fetch('https://randomuser.me/api/', {
          method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
      }).then((response) => {
          response.json().then((data) => {
            this.setState({
              user: data.results[0],
              loading: false
            });
          });
      });
  }

  renderUser() {
    if(this.state.loading){
      return <Spinner size='large' />
    }
   
    return (
      <Card>
        <CardSection>
          <UserCard data={this.state.user} />
        </CardSection>
        <CardSection>
          <Button onPress={this.getUser.bind(this)} title="Random User" color="#4C6BFC" />
        </CardSection>
       </Card>
    );
  }

  componentWillMount(){
    this.getUser();
  }

  render() {

    return (
      <View style={{ flex : 1 }}>
   		 <Header headerText="First Application"/>
       { this.renderUser() }
      </View>
    );
  }
}